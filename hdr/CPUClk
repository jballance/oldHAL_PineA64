; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;
;


        GET   Hdr:CPUClkDevice

; Some TPS registers
TPSPM_IIC           * &4B

DCDC_GLOBAL_CFG     * &61
VDD1_VSEL           * &B9
VDD1_VMODE_CFG      * &BA

; OPP table format
                 ^  0
OPPTbl_MHz       #  2 ; Max MHz value for this voltage
OPPTbl_VDD1      #  1 ; Required VDD1 value, in VDD1_VSEL format
OPPTbl_CLKOUT_M2 #  1 ; Required DPLL1CLKOUT_M2 value
OPPTbl_Size      #  0 ; Size of each entry

OPPTbl_Format    *  0 ; Format number as returned by CPUClk_Override

OPPTbl_Max       *  17 ; Max number of entries we support (chosen to make us same size as SR driver)

; CPU clock device
                   ^  0, a1
; Public bits
CPUClkDevice       #  HALDevice_CPUClk_Size_0_2 ; support API 0.2
; Private bits
CPUClkShutdown     #  4 ; Pointer to shutdown func. Must be at same offset as SR37xShutdown!
CPUClkWorkspace    #  4 ; HAL workspace pointer
CPUClkNewSpeed     #  4 ; Re-entrancy flag. -1 if idle, desired table idx if in process of changing CPU speed. Allows CPUClk_Get to finish changing the speed if it gets called in the middle of a change.
CPUClkCurSpeed     #  4 ; Current table idx
CPUClkOPPTblSize   #  4 ; Number of entries in table
CPUClkOPPTbl       #  OPPTbl_Size*OPPTbl_Max ; OPP table
CPUClkOPPDefault   #  OPPTbl_Size ; Default OPP settings, for shutdown
CPUClk_DeviceSize  *  :INDEX: @

CPUClk_WorkspaceSize  * CPUClk_DeviceSize

        END
